﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBehaviourShoot : StateMachineBehaviour
{
    [SerializeField] private GameObject projectilePrefab;
    private GameObject projectileExit;
    private bool canFire;
    [SerializeField] private float attackrate;
    private float nextAttackTime;
    [SerializeField] private Sprite attackSprite;
    private Sprite defaultSprite;

    [SerializeField] private float timer;
    private float startTimer;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        projectileExit = GameObject.Find("BossProjectileExit");
        defaultSprite = animator.gameObject.GetComponent<SpriteRenderer>().sprite;
        startTimer = timer;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
   {
        if (Time.time >= nextAttackTime)
        {
            Fire();
            animator.GetComponent<SpriteRenderer>().sprite = attackSprite;
            nextAttackTime = Time.time + 1 / attackrate;
        }
        timer -= Time.deltaTime;
        //animator.GetComponent<SpriteRenderer>().sprite = defaultSprite;
        if (timer <= 0)
        {
            animator.SetTrigger("Idle");
            timer = startTimer;
        }
   }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
    void Fire()
    {
        Instantiate(projectilePrefab, projectileExit.transform.position, Quaternion.identity);
    }
}
