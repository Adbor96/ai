﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshPatrol : MonoBehaviour
{
    private NavMeshAgent myAgent;
    [SerializeField] private List<Transform> wayPoints;
    int currentWP;
    // Start is called before the first frame update
    void Start()
    {
        myAgent = GetComponent<NavMeshAgent>();
        currentWP = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Patrol();
        Debug.Log(Vector3.Distance(transform.position, wayPoints[currentWP].position));
    }
    void Patrol()
    {
        if (wayPoints.Count == 0)
            return;

        if(Vector3.Distance(transform.position, wayPoints[currentWP].position) <= 1.5f)
        {
            Debug.Log("Change waypoint");
            currentWP++;
            if(currentWP >= wayPoints.Count)
            {
                currentWP = 0;
            }
        }
        myAgent.SetDestination(wayPoints[currentWP].position);
    }
}
