﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshCapsule : MonoBehaviour
{
    private NavMeshAgent myAgent;
    [SerializeField] private LayerMask walls;
    [SerializeField] private float radius;
    // Start is called before the first frame update
    void Start()
    {
        myAgent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if(Physics.Raycast(ray, out hit))
            {
                myAgent.SetDestination(hit.point);
            }
        }
        Collider[] nearbyWalls = Physics.OverlapSphere(transform.position, radius, walls);
        Vector3 avoidDir = Vector3.zero;
        for (int i = 0; i < nearbyWalls.Length; i++)
        {
            if (Vector3.Distance(nearbyWalls[i].transform.position, transform.position) < 1)
            {
                Vector3 wallDirection = nearbyWalls[i].transform.position - transform.position;
                wallDirection = wallDirection.normalized * (1 - wallDirection.magnitude / 1);
                avoidDir += -wallDirection;
            }
        }
        avoidDir.Normalize();
        transform.position += avoidDir;
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
