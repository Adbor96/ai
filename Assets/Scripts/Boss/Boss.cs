﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss : MonoBehaviour, IDamageable
{
    [SerializeField] private int health;
    [SerializeField] private Slider healthBar;
    [SerializeField] private GameObject projectilePrefab;
    private PlayerFire player;
    private SpriteRenderer rend;

    private Animator myAnim;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindObjectOfType<PlayerFire>();
        rend = GetComponent<SpriteRenderer>();
        myAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        healthBar.value = health;
        if(player.transform.position.x < transform.position.x)
        {
            transform.localScale = new Vector2(-1, 1);
        }
        else
        {
            transform.localScale = new Vector2(1, 1);
        }
    }
    public void TakeDamage()
    {

    }

    public void TakeDamage(int damageAmount)
    {
        health -= damageAmount;
        if(health <= 0)
        {
            myAnim.SetTrigger("Death");
        }
    }
}
