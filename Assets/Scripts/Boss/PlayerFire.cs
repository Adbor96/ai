﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFire : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float jumpForce;
    private SpriteRenderer spriteRend;
    private Rigidbody2D rb;
    private float x;
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private Transform bulletExit;
    [SerializeField] private LayerMask ground;

    private float fallMultiplier = 2.5f;
    private float lowJumpMultiplier = 2f;

    [SerializeField] private int health;

    private CircleCollider2D myCollider;
    private bool isGrounded;
    // Start is called before the first frame update
    void Start()
    {
        spriteRend = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        myCollider = GetComponent<CircleCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        x = Input.GetAxisRaw("Horizontal");
        if (Input.GetMouseButtonDown(0))
        {
            Fire();
        }
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector2 movement = new Vector2(moveHorizontal, 0);
        rb.AddForce(movement * speed);
        if (Input.GetButtonDown("Jump") && IsGrounded())
        {
            rb.velocity = Vector2.up * jumpForce;
        }
        if (rb.velocity.y < 0)
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }
        else if (rb.velocity.y > 0 && !Input.GetButton("Jump"))
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
        }
    }
    private void FixedUpdate()
    {

    }

    void Fire()
    {
        Instantiate(bulletPrefab, bulletExit.position, Quaternion.identity);
    }
    void FlipSprite()
    {
        if (x < 0)
        {
            spriteRend.flipX = true;
        }
        else if (x > 0)
        {
            spriteRend.flipX = false;
        }
    }
    private bool IsGrounded()
    {
        return transform.Find("GroundCheck").GetComponent<GroundCheck>().isGrounded;
    }
    public void Damage(int damageAMount)
    {
        health -= damageAMount;
    }
}
