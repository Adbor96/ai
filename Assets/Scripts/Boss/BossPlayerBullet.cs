﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossPlayerBullet : MonoBehaviour
{
    private Vector3 worldPosition;
    [SerializeField] private float speed;
    [SerializeField] private int damageAmount;
    Vector3 direction;
    // Start is called before the first frame update
    void Start()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = Camera.main.nearClipPlane;
        worldPosition = Camera.main.ScreenToWorldPoint(mousePos);
        direction = (worldPosition - transform.position).normalized;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += direction * speed * Time.deltaTime;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        IDamageable enemy = collision.gameObject.GetComponent<IDamageable>();
        if(enemy != null)
        {
            Debug.Log("Damage");
            enemy.TakeDamage(damageAmount);
            Destroy(gameObject);
        }
    }
}
