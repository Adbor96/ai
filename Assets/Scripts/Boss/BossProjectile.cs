﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossProjectile : MonoBehaviour
{
    Vector3 playerPosition;
    PlayerFire player;
    [SerializeField] private float speed;
    [SerializeField] private int damage;
    [SerializeField] private float timer;
    private Rigidbody2D myRb;
    Vector3 spellDirection;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindObjectOfType<PlayerFire>();
        playerPosition = player.transform.position;
        myRb = GetComponent<Rigidbody2D>();
        spellDirection = (playerPosition - transform.position).normalized;
    }

    // Update is called once per frame
    void Update()
    {
        //Vector2 difference = playerPosition - (Vector2)transform.position;
        //float distance = difference.magnitude;
        //Vector2 direction = difference / distance;
        //direction.Normalize();
        //myRb.velocity = direction * speed;
        


        
        /* if (Vector2.Distance(transform.position, playerPosition) <= 0.5f)
         {
             Destroy(gameObject);
         }*/
        transform.position += spellDirection * speed * Time.deltaTime;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        PlayerFire player = collision.gameObject.GetComponent<PlayerFire>();
        Debug.Log(collision);
        if(player != null)
        {
            player.Damage(damage);
        }
        if(collision.gameObject.name != "Boss")
        {
            Destroy(gameObject);
        }
    }
    private void CountDownToDestroy()
    {
        timer -= Time.deltaTime;
        if(timer <= 0)
        {
            Destroy(gameObject);
        }
    }
}
