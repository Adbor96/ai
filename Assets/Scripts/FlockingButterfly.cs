﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockingButterfly : MonoBehaviour
{
    Rigidbody2D rb;
    List<GameObject> neighbours;
    public float sightRadius = 5f;
    public float fieldOfView = 360f;
    public float separationRange = 2f;

    public float alignmentWeight = 1f;
    public float cohesionWeight = 1f;
    public float separationWeight = 1f;

    public float avoidLeavingScreenWeight = 0f;
    public float moveForwardWeight = 0f;

    public float moveSpeed = 1f;
    // Start is called before the first frame update
    void Start()
    {
        //Setup
        rb = GetComponent<Rigidbody2D>();
        neighbours = new List<GameObject>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Kolla vilka vi ser
        neighbours.Clear();
        foreach (Collider2D col in  Physics2D.OverlapCircleAll(transform.position, sightRadius))
        {
            if (col.gameObject != gameObject)
            {
                Vector2 dirToNeighbour = (col.transform.position - transform.position).normalized;
                if (Vector2.Dot(transform.up, dirToNeighbour) > 1 - fieldOfView / 180)
                {
                    neighbours.Add(col.gameObject);
                }
            }
        }
   
        //Räkna ut åt vilket håll vi vill åka genom att lägga ihop alla steering behaviours.
        Vector2 moveDirection = Vector2.zero;
        //De tre vanliga, Cohesion, Separation och Alignment
        moveDirection += Cohesion() * cohesionWeight;
        moveDirection += Separation() * separationWeight;
        moveDirection += Alignment()* alignmentWeight;

        //Några extra jag testade
        moveDirection += AvoidLeavingScreen() * avoidLeavingScreenWeight;
        moveDirection += (Vector2)transform.up * moveForwardWeight;

        moveDirection.Normalize();
        //Få den att faktiskt åka åt rätt håll. Även en utjämning för att undvika alltför ryckiga rörelser.
        rb.velocity =  Vector2.Lerp(rb.velocity, moveDirection * moveSpeed, 0.1f);
        transform.up = Vector2.Lerp(transform.up, moveDirection, 0.1f);
    }

    Vector2 Cohesion()
    {
        Vector2 center = Vector2.zero;
        for (int i = 0; i < neighbours.Count; i ++)
        {
            center += (Vector2)neighbours[i].transform.position;
        }
        center /= neighbours.Count;

        Vector2 dir = center - (Vector2)transform.position;
        dir.Normalize();
        Debug.DrawRay(transform.position,dir, Color.green);
        
        return dir;
        
    }

    Vector2 Separation()
    {
        Vector2 dir = Vector2.zero;
        for (int i = 0; i < neighbours.Count; i ++)
        {
            if (Vector2.Distance(neighbours[i].transform.position,transform.position) < separationRange)
            {
                Vector2 neighbourDir = (Vector2)neighbours[i].transform.position - (Vector2)transform.position;
                neighbourDir = neighbourDir.normalized * (separationRange -neighbourDir.magnitude)/separationRange;
                dir += -neighbourDir;
            }
        }
        dir = Vector2.ClampMagnitude(dir, 1); //istället för normalize för att acceptera kortare magnitud än 1.
        //dir.Normalize();
        Debug.DrawRay(transform.position, dir,Color.red);
        return dir;
    }

    Vector2 Alignment()
    {
        Vector2 dir = Vector2.zero;
        for (int i = 0; i < neighbours.Count; i ++)
        {
            dir += neighbours[i].GetComponent<Rigidbody2D>().velocity;
        }
        dir.Normalize();
        Debug.DrawRay(transform.position, dir, Color.blue);
        return dir;
    }

    Vector2 AvoidLeavingScreen()
    {
        Vector2 dir = Vector2.zero;
        Vector2 screenPos = Camera.main.WorldToScreenPoint(transform.position);
        if (screenPos.y > Screen.height)
        {
            dir += new Vector2(0,-1);
        }
        if (screenPos.y < 0)
        {
            dir += new Vector2(0, 1);
        }
        if (screenPos.x > Screen.width)
        {
            dir += new Vector2(-1,0);
        }
        if (screenPos.x < 0)
        {
            dir += new Vector2(1, 0);
        }
        dir.Normalize();
        Debug.DrawRay(transform.position,dir, Color.white,1f); 
        return dir;
    }
}
