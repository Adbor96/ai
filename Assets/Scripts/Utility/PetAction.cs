﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetAction : MonoBehaviour
{
    public virtual float CalculateUtilityScore(PetAgent agent)
    {
        return -1;
    }

    //referera till agenten så att den vet vem som utför actionen
    public virtual void DoAction(PetAgent agent)
    {

    }
}
