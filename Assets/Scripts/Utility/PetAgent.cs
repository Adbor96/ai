﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PetAgent : MonoBehaviour
{
    [SerializeField] private PetObject petData;

    //den handling karaktären gör just nu
    public PetAction currentAction;

    //handlingar karaktären kan göra
    public List<PetAction> availableActions;

    public float tiredLevel;
    [SerializeField] private Image tiredBar;
    public float hungerLevel;
    [SerializeField] private Image hungerBar;
    public float thirstLevel;
    [SerializeField] private Image thirstBar;
    public float boredLevel;
    [SerializeField] private Image boredBar;

    public bool isSick;

    public Color defaultColor;
    private void Start()
    {
        tiredLevel = petData.tiredLevel;
        hungerLevel = petData.hungerLevel;
        thirstLevel = petData.thirstLevel;
        boredLevel = petData.boredLevel;
        defaultColor = GetComponent<SpriteRenderer>().color;
    }
    private void Update()
    {
        tiredLevel += Time.deltaTime * 0.02f;
        hungerLevel += Time.deltaTime * 0.02f;
        boredLevel += Time.deltaTime * 0.03f;
        thirstLevel += Time.deltaTime * 0.03f;
        hungerBar.fillAmount = hungerLevel;
        tiredBar.fillAmount = tiredLevel;
        thirstBar.fillAmount = thirstLevel;
        boredBar.fillAmount = boredLevel;

        //om agenten inte har något att göra
        if(currentAction == null)
        {
            ChooseAction(); 
        }
        else
        {
            currentAction.DoAction(this);
        }
    }
    public void MoveTowards(Vector2 pos)
    {
        transform.position = Vector2.MoveTowards(transform.position, pos, petData.speed * Time.deltaTime);
    }
    void ChooseAction()
    {
        FindActions();
        //Debug.Log("Choosing new action");
        float bestScore = 0;
        PetAction bestAction = null;
        //gå igenom alla actions
        for (int i = 0; i < availableActions.Count; i++)
        {
            float score = availableActions[i].CalculateUtilityScore(this);
            if (score > bestScore)
            {
                bestScore = score;
                bestAction = availableActions[i];
            }
        }
        currentAction = bestAction;
    }
    void FindActions()
    {
        availableActions.Clear();
        PetAction[] activeActions = FindObjectsOfType<PetAction>();
        foreach (PetAction action in activeActions)
        {
            availableActions.Add(action);
        }
    }
    public void Healthy()
    {
        GetComponent<SpriteRenderer>().color = defaultColor;
    }
}
