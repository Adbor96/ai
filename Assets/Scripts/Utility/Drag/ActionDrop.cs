﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ActionDrop : MonoBehaviour, IDropHandler
{
    private ActionManager manager;
    private void Start()
    {
        manager = GameObject.FindObjectOfType<ActionManager>();
    }
    public void OnDrop(PointerEventData eventData)
    {

        Debug.Log("Drop");
        ActionDrag draggedItem = eventData.pointerDrag.GetComponent<ActionDrag>();
        if (draggedItem != null)
        {
            //eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = GetComponent<RectTransform>().anchoredPosition;
            switch (eventData.pointerDrag.name)
            {
                case "Apple":
                    if(this.name == "Food Drop")
                    {
                        draggedItem.GetComponent<RectTransform>().position = draggedItem.defaultPos;
                        PetAction food = Instantiate(manager.Food, GetComponent<RectTransform>().position, Quaternion.identity);
                        food.GetComponent<SpriteRenderer>().sortingOrder = 1;
                    }
                    else
                    {
                        Debug.Log("Wrong slot");
                        draggedItem.GetComponent<RectTransform>().position = draggedItem.defaultPos;
                    }
                    break;

                case "Water":
                    if(this.name == "Water Drop")
                    {
                        draggedItem.GetComponent<RectTransform>().position = draggedItem.defaultPos;
                        PetAction water = Instantiate(manager.Water, GetComponent<RectTransform>().position, Quaternion.identity);
                        water.GetComponent<SpriteRenderer>().sortingOrder = 1;
                    }
                    else
                    {
                        Debug.Log("Wrong slot");
                        draggedItem.GetComponent<RectTransform>().position = draggedItem.defaultPos;
                    }
                    break;

                case "Game":
                    if (this.name == "Game Drop")
                    {
                        draggedItem.GetComponent<RectTransform>().position = draggedItem.defaultPos;
                        PetAction game = Instantiate(manager.Game, GetComponent<RectTransform>().position, Quaternion.identity);
                        game.GetComponent<SpriteRenderer>().sortingOrder = 1;
                    }
                    else
                    {
                        Debug.Log("Wrong slot");
                        draggedItem.GetComponent<RectTransform>().position = draggedItem.defaultPos;
                    }
                    break;
            }

        }
    }
}
