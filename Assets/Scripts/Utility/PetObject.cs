﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pets/Pet")]
public class PetObject : ScriptableObject
{
    public float tiredLevel;
    public float hungerLevel;
    public float thirstLevel;
    public float boredLevel;
    public float speed;
}
