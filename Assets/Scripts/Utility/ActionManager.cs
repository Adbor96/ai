﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionManager : MonoBehaviour
{
    public PetAction Water; 
    public PetAction Game; 
    public PetAction Food;
}
