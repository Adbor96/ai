﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drink : PetAction
{
    //hur lång tid det ska ta att dricka
    [SerializeField] private float drinkingTime;
    //hur lång tid agenten har druckit
    float drinkingProgress;
    //hur mycket törst går ner genom att dricka
    [SerializeField] private float thirsReduction;
    [SerializeField] private AnimationCurve utilCurve;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public override float CalculateUtilityScore(PetAgent agent)
    {
        float score = utilCurve.Evaluate(agent.thirstLevel);
        return score;
    }
    public override void DoAction(PetAgent agent)
    {
        //om agenten inte är nära vattnet rör den sig mot den
        if (Vector2.Distance(agent.transform.position, transform.position) > 0.5f)
        {
            agent.MoveTowards(transform.position);
        }
        else //om den är nära vattnet börjar den dricka
        {
            Debug.Log("drinking");
            drinkingProgress += Time.deltaTime;
            if (drinkingProgress > drinkingTime) //om agenten har ätit tillräckligt länge
            {
                //agenten blir mindre hungrig
                agent.thirstLevel -= thirsReduction;
                if (agent.thirstLevel <= 0)
                {
                    agent.thirstLevel = 0;
                }

                //agenten har inget mer att göra just nu
                agent.currentAction = null;
                drinkingProgress = 0;
                agent.availableActions.Remove(this);
                Destroy(gameObject);
            }
        }
    }
}
