﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Play : PetAction
{
    [SerializeField] private float playSpeed = 1f;
    public override float CalculateUtilityScore(PetAgent agent)
    {
        float score = agent.boredLevel;
        return score;
    }
    public override void DoAction(PetAgent agent)
    {
        //om agenten inte är nära spelet rör den sig mot den
        if (Vector2.Distance(agent.transform.position, transform.position) > 0.5f)
        {
            agent.MoveTowards(transform.position);
        }
        else //om den är nära spelet börjar den leka
        {
            agent.boredLevel -= playSpeed * Time.deltaTime;
            if (agent.boredLevel <= 0)
            {
                agent.boredLevel = 0;
                agent.currentAction = null;
                agent.availableActions.Remove(this);
                Destroy(gameObject);
            }
        }
    }
}
