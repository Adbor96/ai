﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eat : PetAction
{

    //hur lång tid det ska ta att äta
    [SerializeField] private float eatingTime;
    //hur lång tid agenten har ätit
    float eatingProgress;
    //hur mycket hunger går ner genom att äta
    [SerializeField] private float hungerReduction;
    [SerializeField] private AnimationCurve utilCurve;
    [SerializeField] private float spoilTime;
    public bool isRotten;
    private Color defaultColor;
    private Color rottenColor;
    private void Awake()
    {
        defaultColor = GetComponent<SpriteRenderer>().color;
        rottenColor = new Color(48, 132, 56);
    }
    private void Update()
    {
        Spoil();
    }
    public override float CalculateUtilityScore(PetAgent agent)
    {
        float score = utilCurve.Evaluate(agent.hungerLevel);
        //float score = agent.hungerLevel;
        return score;
    }
    public override void DoAction(PetAgent agent)
    {
        //om agenten inte är nära maten rör den sig mot den
        if (Vector2.Distance(agent.transform.position, transform.position) > 0.5f)
        {
            agent.MoveTowards(transform.position);
        }
        else //om den är nära maten börjar den äta
        {
            Debug.Log("Eating");
            eatingProgress += Time.deltaTime;
            if(eatingProgress > eatingTime) //om agenten har ätit tillräckligt länge
            {
                //agenten blir mindre hungrig
                agent.hungerLevel -= hungerReduction;
                if (agent.hungerLevel <= 0)
                {
                    agent.hungerLevel = 0;
                }

                //agenten har inget mer att göra just nu
                agent.currentAction = null;
                eatingProgress = 0;
                if (isRotten)
                {
                    agent.isSick = true;
                    agent.GetComponent<SpriteRenderer>().color = Color.green;
                }
                agent.availableActions.Remove(this);
                Destroy(gameObject);
            }
        }
    }
    private void Spoil()
    {
        spoilTime -= Time.deltaTime;
        if(spoilTime <= 0)
        {
            isRotten = true;
            GetComponent<SpriteRenderer>().color = Color.green;
        }
    }
}
