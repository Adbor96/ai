﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sleep : PetAction
{
    [SerializeField] private float restSpeed = 1f;
    [SerializeField] private float maxTired;
    [SerializeField] private AnimationCurve utilCurve;
    private float score;
    public override float CalculateUtilityScore(PetAgent agent)
    {
        score = utilCurve.Evaluate(agent.tiredLevel);
        //float score = agent.tiredLevel;
        return score;
    }
    public override void DoAction(PetAgent agent)
    {
        Debug.Log(score);
        if(score >= maxTired || agent.isSick)
        {
            //om agenten inte är nära sängen rör den sig mot den
            if (Vector2.Distance(agent.transform.position, transform.position) > 0.5f)
            {
                agent.MoveTowards(transform.position);
            }
        }

        if (Vector2.Distance(agent.transform.position, transform.position) < 0.5f) //om den är nära sängen börjar den sova
        {
            agent.tiredLevel -= restSpeed * Time.deltaTime;
            if (agent.tiredLevel <= 0)
            {
                agent.tiredLevel = 0;
                agent.currentAction = null;
                agent.isSick = false;
                agent.GetComponent<SpriteRenderer>().color = agent.defaultColor;
            }
        }
    }
}
