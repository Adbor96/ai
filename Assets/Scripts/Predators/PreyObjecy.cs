﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Animals/Prey")]
public class PreyObjecy : ScriptableObject
{
    public float sightRadius;
    public float avoidRadius;

    public float flyToMouseWeight;
    public float cohesionWeight;
    public float avoidanceWeight;
    public float alignmentWeight;
    public float chasingPreyWeight;
    public float fleeWeight;
    public float avoidLeavingScreenWeight;
    public float wanderWeight;

    public float moveSpeed;

    public float fieldOfView;
}
