﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Predator : MonoBehaviour
{
    private Rigidbody2D rb;
    private SpriteRenderer bearSprite;

    private List<GameObject> nearbyPredators;
    private List<GameObject> nearbyPrey;
    //kolla efter närvarande rovdjur
    [SerializeField] private float sightRadius;
    [SerializeField] private float avoidRadius;

    [Range(0, 360)] [SerializeField] private float fieldOfView = 360f;
    
    //kolla efter närvarande bytesdjur
    [Range(0, 20)] [SerializeField] private float preyRadius;

    [SerializeField] private float wanderWeight;
    [SerializeField] private float maxDistanceX;
    [SerializeField] private float maxDistanceY;
    [SerializeField] private float minDistanceX;
    [SerializeField] private float minDistanceY;

    [SerializeField] private float flyToMouseWeight;
    [SerializeField] private float cohesionWeight;
    [SerializeField] private float avoidanceWeight;
    [SerializeField] private float alignmentWeight;
    [SerializeField] private float chasingPreyWeight;
    [SerializeField] private float avoidLeavingScreenWeight;
   
    [SerializeField] private float moveSpeed;
    private bool facingRight;



    // Start is called before the first frame update
    void Start()
    {
        nearbyPredators = new List<GameObject>();
        nearbyPrey = new List<GameObject>();

        rb = GetComponent<Rigidbody2D>();
        bearSprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void FixedUpdate()
    {
        nearbyPredators.Clear();
        nearbyPrey.Clear();
        //kolla efter närvarande djur
        foreach (Collider2D animals in Physics2D.OverlapCircleAll(transform.position, sightRadius))
        {
            //exkludera sig själv från kollen
            //if (animals != gameObject)
            //{
            Predator predator = animals.GetComponent<Predator>();
            Prey prey = animals.GetComponent<Prey>();

            //field of View
            Vector2 dirToNeighbour = (animals.transform.position - transform.position).normalized;
            if (Vector2.Dot(transform.right, dirToNeighbour) > 1 - fieldOfView / 180)
            {
                //om ett djur nära är ett rovdjur, lägg till det i rovdjurslistan
                if (predator != null && predator != this)
                {
                    nearbyPredators.Add(animals.gameObject);
                    Debug.DrawRay(transform.position, animals.transform.position, Color.white);
                }
                //om ett djur nära är ett bytesdjur, lägg till det i bytesdjurslistan
                if (prey != null)
                {
                    nearbyPrey.Add(animals.gameObject);
                }
            }

            //}
        }

        Vector2 moveDirection = Vector2.zero;
        moveDirection += FlyToMouse() * flyToMouseWeight;
        moveDirection += Wander() * wanderWeight;
        moveDirection += Cohesion() * cohesionWeight;
        moveDirection += Avoidance() * avoidanceWeight;
        moveDirection += Alignment() * alignmentWeight;
        moveDirection += Chase() * chasingPreyWeight;
        moveDirection += AvoidLeavingScreen() * avoidLeavingScreenWeight;

        moveDirection.Normalize();
        rb.velocity = moveDirection * moveSpeed;
        //transform.right = moveDirection;
        //Debug.Log(rb.velocity);
        if(rb.velocity.x > 0)
        {
            bearSprite.flipX = false;
        }
        else
        {
            bearSprite.flipX = true;
        }
        //transform.LookAt((Vector2)transform.position + moveDirection);

    }
    Vector2 FlyToMouse()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 direction = mousePos - (Vector2)transform.position;
        return direction.normalized;
    }
    private Vector2 Wander()
    {
        Vector2 newTarget = new Vector2(Random.Range(minDistanceX, maxDistanceX), Random.Range(minDistanceY, maxDistanceY));
        return newTarget;
    }
    private Vector2 Cohesion()
    {
        //medelvärdet av de närvarande rovdjuren
        Vector2 center = Vector2.zero;
        int neighbours = 0;
        for (int i = 0; i < nearbyPredators.Count; i++)
        {
            neighbours++;
            center += (Vector2)nearbyPredators[i].transform.position;
        }
        //få mellanvärdet av alla karaktärers position
        center /= neighbours;

        //skapa en riktning av dit rovdjuret ska röra sig(i detta fall iväg från andra rovdjur)
        Vector2 direction = center - (Vector2)transform.position;
        Debug.DrawRay(transform.position, direction, Color.grey);
        return direction.normalized;
    }
        private Vector2 Avoidance()
    {
        Vector2 dir = Vector2.zero;
        for (int i = 0; i < nearbyPredators.Count; i++)
        {
            if (Vector2.Distance(nearbyPredators[i].transform.position, transform.position) < avoidRadius)
            {
                Vector2 neighbourDir = (Vector2)nearbyPredators[i].transform.position - (Vector2)transform.position;
                //vector2's längd ska vara olika starka beroende på hur nära grannarna är
                //neighbourDir ska bli ett värde mellan 0 och 1. 1 är väldigt nära, 0 är precis ute vid kanten
                neighbourDir = neighbourDir.normalized * (1 - neighbourDir.magnitude / avoidRadius);
                dir += -neighbourDir;
            }
        }

        dir.Normalize();
        Debug.DrawRay(transform.position, dir, Color.red);
        return dir;
    }
        private Vector2 Alignment()
    {
        Vector2 dir = Vector2.zero;
        for (int i = 0; i < nearbyPredators.Count; i++)
        {
            dir += nearbyPredators[i].GetComponent<Rigidbody2D>().velocity;
        }
        dir.Normalize();
        Debug.DrawRay(transform.position, dir, Color.green);
        return dir;
    }
        private Vector2 Chase()
    {
        Vector2 dir = Vector2.zero;
        float nearestPreyDistance = sightRadius;
        Vector2 nearestPreyPosition = Vector2.zero;
        for (int i = 0; i < nearbyPrey.Count; i++)
        {
            dir = nearbyPrey[i].transform.position - transform.position;
            if(Vector2.Distance(transform.position, nearbyPrey[i].transform.position) < nearestPreyDistance)
            {
                nearestPreyDistance = Vector2.Distance(transform.position, nearbyPrey[i].transform.position);
                nearestPreyPosition = nearbyPrey[i].transform.position;
            }
            if (Vector2.Distance(transform.position, nearbyPrey[i].transform.position) < 1)
            {
                nearbyPredators.Remove(nearbyPrey[i].gameObject);
            }
        }
        Vector2 chaseDir = nearestPreyPosition - (Vector2)transform.position;
        return chaseDir.normalized;
    }
    Vector2 AvoidLeavingScreen()
    {
        Vector2 dir = Vector2.zero;
        Vector2 screenPos = Camera.main.WorldToScreenPoint(transform.position);
        if (screenPos.y > Screen.height)
        {
            dir += new Vector2(0, -1);
        }
        if (screenPos.y < 0)
        {
            dir += new Vector2(0, 1);
        }
        if (screenPos.x > Screen.width)
        {
            dir += new Vector2(-1, 0);
        }
        if (screenPos.x < 0)
        {
            dir += new Vector2(1, 0);
        }
        dir.Normalize();
        Debug.DrawRay(transform.position, dir, Color.white, 1f);
        return dir;
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, sightRadius);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, avoidRadius);
    }

}
