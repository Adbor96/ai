﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prey : MonoBehaviour
{
    private Rigidbody2D rb;
    private SpriteRenderer deerSprite;

    private List<GameObject> nearbyPredators;
    private List<GameObject> nearbyPrey;
    //kolla efter närvarande rovdjur


    //kolla efter närvarande bytesdjur

    [SerializeField] private float maxDistanceX;
    [SerializeField] private float maxDistanceY;
    [SerializeField] private float minDistanceX;
    [SerializeField] private float minDistanceY;

    private Vector2 targetPos;

    private float positionCheckTimer = 10;

    private bool facingRight;

    [SerializeField] private PreyObjecy preyData;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        deerSprite = GetComponent<SpriteRenderer>();

        nearbyPredators = new List<GameObject>();
        nearbyPrey = new List<GameObject>();

        targetPos = RandomPosition();
        Debug.Log(RandomPosition());
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(targetPos);
    }
    private void FixedUpdate()
    {
        nearbyPredators.Clear();
        nearbyPrey.Clear();
        //kolla efter närvarande djur
        foreach (Collider2D animals in Physics2D.OverlapCircleAll(transform.position, preyData.sightRadius))
        {
            //exkludera sig själv från kollen
            //if (animals != gameObject)
            //{
            Predator predator = animals.GetComponent<Predator>();
            Prey prey = animals.GetComponent<Prey>();

            //field of View
            Vector2 dirToNeighbour = (animals.transform.position - transform.position).normalized;
            if (Vector2.Dot(-transform.right, dirToNeighbour) > 1 - preyData.fieldOfView / 180)
            {
                //om ett djur nära är ett rovdjur, lägg till det i rovdjurslistan
                if (predator != null)
                {
                    nearbyPredators.Add(animals.gameObject);
                    Debug.DrawRay(transform.position, animals.transform.position, Color.white);
                }
                //om ett djur nära är ett bytesdjur, lägg till det i bytesdjurslistan
                if (prey != null && prey != this)
                {
                    nearbyPrey.Add(animals.gameObject);
                }
            }

            //}
            Vector2 moveDirection = Vector2.zero;
            moveDirection += FlyToMouse() * preyData.flyToMouseWeight;
            moveDirection += FollowTarget() * preyData.wanderWeight;
            moveDirection += Cohesion() * preyData.cohesionWeight;
            moveDirection += Avoidance() * preyData.avoidanceWeight;
            moveDirection += Alignment() * preyData.alignmentWeight;
            moveDirection += Flee() * preyData.fleeWeight;
            moveDirection += AvoidLeavingScreen() * preyData.avoidLeavingScreenWeight;

            moveDirection.Normalize();
            rb.velocity = moveDirection * preyData.moveSpeed;
        }
    }
    Vector2 FlyToMouse()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 direction = mousePos - (Vector2)transform.position;
        return direction.normalized;
    }
    Vector2 FollowTarget()
    {
        if (Vector2.Distance(transform.position, targetPos) < 1)
        {
            Debug.Log(Vector2.Distance(transform.position, targetPos));
            targetPos = RandomPosition();
        }
        Vector2 direction = targetPos - (Vector2)transform.position;
        Debug.DrawRay(transform.position, direction, Color.cyan);
        Debug.DrawLine(transform.position, targetPos, Color.black);
        return direction.normalized;
    }
    private Vector2 Wander()
    {
        Vector2 newTarget = new Vector2(Random.Range(minDistanceX, maxDistanceX), Random.Range(minDistanceY, maxDistanceY));
        return newTarget;
    }
    private Vector2 Cohesion()
    {
        //medelvärdet av de närvarande rovdjuren
        Vector2 center = Vector2.zero;
        int neighbours = 0;
        for (int i = 0; i < nearbyPrey.Count; i++)
        {
            neighbours++;
            center += (Vector2)nearbyPrey[i].transform.position;
        }
        //få mellanvärdet av alla karaktärers position
        center /= neighbours;

        //skapa en riktning av dit rovdjuret ska röra sig(i detta fall iväg från andra rovdjur)
        Vector2 direction = center - (Vector2)transform.position;
        Debug.DrawRay(transform.position, direction, Color.grey);
        return direction.normalized;
    }
    private Vector2 Avoidance()
    {
        Vector2 dir = Vector2.zero;
        for (int i = 0; i < nearbyPrey.Count; i++)
        {
            if (Vector2.Distance(nearbyPrey[i].transform.position, transform.position) < preyData.avoidRadius)
            {
                Vector2 neighbourDir = (Vector2)nearbyPrey[i].transform.position - (Vector2)transform.position;
                //vector2's längd ska vara olika starka beroende på hur nära grannarna är
                //neighbourDir ska bli ett värde mellan 0 och 1. 1 är väldigt nära, 0 är precis ute vid kanten
                neighbourDir = neighbourDir.normalized * (1 - neighbourDir.magnitude / preyData.avoidRadius);
                dir += -neighbourDir;
            }
        }

        dir.Normalize();
        Debug.DrawRay(transform.position, dir, Color.red);
        return dir;
    }
    private Vector2 Alignment()
    {
        Vector2 dir = Vector2.zero;
        for (int i = 0; i < nearbyPrey.Count; i++)
        {
            dir += nearbyPrey[i].GetComponent<Rigidbody2D>().velocity;
        }
        dir.Normalize();
        Debug.DrawRay(transform.position, dir, Color.green);
        return dir;
    }
    private Vector2 Flee()
    {
        Vector2 dir = Vector2.zero;
        for (int i = 0; i < nearbyPredators.Count; i++)
        {
            if (Vector2.Distance(nearbyPredators[i].transform.position, transform.position) < preyData.avoidRadius)
            {
                Vector2 neighbourDir = (Vector2)nearbyPredators[i].transform.position - (Vector2)transform.position;
                //vector2's längd ska vara olika starka beroende på hur nära grannarna är
                //neighbourDir ska bli ett värde mellan 0 och 1. 1 är väldigt nära, 0 är precis ute vid kanten
                neighbourDir = neighbourDir.normalized * (1 - neighbourDir.magnitude / preyData.avoidRadius);
                dir += -neighbourDir;
            }
        }

        dir.Normalize();
        Debug.DrawRay(transform.position, dir, Color.red);
        return dir;
    }
    Vector2 AvoidLeavingScreen()
    {
        Vector2 dir = Vector2.zero;
        Vector2 screenPos = Camera.main.WorldToScreenPoint(transform.position);
        if (screenPos.y > Screen.height)
        {
            dir += new Vector2(0, -1);
        }
        if (screenPos.y < 0)
        {
            dir += new Vector2(0, 1);
        }
        if (screenPos.x > Screen.width)
        {
            dir += new Vector2(-1, 0);
        }
        if (screenPos.x < 0)
        {
            dir += new Vector2(1, 0);
        }
        dir.Normalize();
        Debug.DrawRay(transform.position, dir, Color.white, 1f);
        return dir;
    }
    Vector2 RandomPosition()
    {
        float randomX = Random.Range(minDistanceX, maxDistanceX);
        float randomY = Random.Range(minDistanceY, maxDistanceY);
        Vector2 position = new Vector2(randomX, randomY);
        return position;
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, preyData.sightRadius);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, preyData.avoidRadius);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Predator predator = collision.gameObject.GetComponent<Predator>();
        if (predator != null)
        {
            GetComponent<Collider2D>().enabled = false;
            deerSprite.enabled = false;
            rb.velocity = Vector2.zero;
            this.enabled = false;
        }
    }
}
