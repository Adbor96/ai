﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum StealthEnemyState { patrol, chaseToArrest, chaseToAttack}
public class StealthEnemy : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private Transform[] patrolPoints;
    [SerializeField] private Transform viewPoint;
    private bool facingRight = true;

    private float timeBtwShot;
    [SerializeField] private float startTimeBtwShot;
    StealthEnemyState state;
    private SpriteRenderer spriteRend;

    [SerializeField] private float backToPatrolTime;
    private float detectionTime;
    [SerializeField] private float viewDistance;
    private Transform player;
    // Start is called before the first frame update
    void Start()
    {
        state = StealthEnemyState.patrol;
        spriteRend = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case StealthEnemyState.patrol:
                if (facingRight)
                {
                    spriteRend.flipX = false;
                    transform.Translate(Vector2.right * speed * Time.deltaTime);
                    if(Vector2.Distance(transform.position, patrolPoints[1].position) < 1)
                    {
                        facingRight = false;
                    }
                    Debug.DrawRay(viewPoint.position, Vector2.right * viewDistance);
                    RaycastHit2D viewRay = Physics2D.Raycast(viewPoint.position, Vector2.right, viewDistance);
                    if (viewRay.collider == true)
                    {
                        Debug.Log(viewRay.collider.name);
                        detectionTime = Vector2.Distance(transform.position, viewRay.collider.transform.position) / 2;
                        detectionTime -= 3;
                        if (detectionTime <= 0)
                        {
                            player = viewRay.collider.transform;
                            state = StealthEnemyState.chaseToArrest;
                        }
                    }
                }
                else
                {
                    Debug.DrawRay(viewPoint.position, Vector2.left * viewDistance);
                    RaycastHit2D viewRay = Physics2D.Raycast(viewPoint.position, Vector2.left, viewDistance);
                    spriteRend.flipX = true;
                    transform.Translate(Vector2.left * speed * Time.deltaTime);
                    if (viewRay.collider == true)
                    {
                        Debug.Log(viewRay.collider.name);
                        detectionTime = Vector2.Distance(transform.position, viewRay.collider.transform.position) / 2;
                        detectionTime -= 3;
                        if (detectionTime <= 0)
                        {
                            player = viewRay.collider.transform;
                            state = StealthEnemyState.chaseToArrest;
                        }
                    }
                    if (Vector2.Distance(transform.position, patrolPoints[0].position) < 1)
                    {
                        facingRight = true;
                    }
                }
                break;
            case StealthEnemyState.chaseToArrest:
                Vector2 directionToPlayer = player.transform.position - transform.position;
                transform.Translate(directionToPlayer.normalized * speed * Time.deltaTime);
                if(Vector2.Distance(transform.position, player.position) <= 0.5f)
                {
                    player.gameObject.GetComponent<Player2>().Arrested();
                }
                break;
        }
    }
}
