﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goomba : MonoBehaviour
{
    private bool movingLeft = true;
    Rigidbody2D rb;
    [SerializeField] private float moveSpeed;
    [SerializeField] private LayerMask mask;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (movingLeft)
        {
            Vector2 moveLeft = new Vector2(1 * moveSpeed * Time.deltaTime, 0);
            rb.velocity = Vector2.left * moveSpeed;
            Debug.DrawRay(transform.position, Vector2.left * 2, Color.red);
            if (Physics2D.Raycast(transform.position, Vector2.left, 2, mask))
            {
                Debug.Log("Hit wall");
                movingLeft = false;
            }
        }
        else
        {
            rb.velocity = Vector2.right * moveSpeed;
            Debug.DrawRay(transform.position, Vector2.right * 2, Color.red);
            if (Physics2D.Raycast(transform.position, Vector2.right, 2, mask))
            {
                Debug.Log("Hit wall");
                movingLeft = true;
            }
        }
    }
}
