﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KoopaRed : MonoBehaviour
{
    private bool movingLeft = true;
    Rigidbody2D rb;
    [SerializeField] private float moveSpeed;
    [SerializeField] private LayerMask[] masks;
    [SerializeField] private LayerMask wallMask;
    [SerializeField] private LayerMask pitMask;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (movingLeft)
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.left, 2);
            Vector2 moveLeft = new Vector2(1 * moveSpeed * Time.deltaTime, 0);
            rb.velocity = Vector2.left * moveSpeed;
            Debug.DrawRay(transform.position, Vector2.left * 2, Color.red);
            if (hit.collider != null)
            {
                /*switch (hit.collider.tag)
                {
                    case "Wall":
                        Debug.Log("Hit wall");
                        movingLeft = false;
                        break;

                    case "Pit":
                        Debug.Log("Pit");
                        movingLeft = false;
                        break;

                    default:
                        break;
                }*/
            }
            if (Physics2D.Raycast(transform.position, Vector2.left, 2, wallMask)) 
             {
                 Debug.Log("Hit wall");
                 movingLeft = false;
             }
             if (Physics2D.Raycast(transform.position, Vector2.left, 1, pitMask))
             {
                 Debug.Log("Hit pit");
                 movingLeft = false;
             }
        }
        else
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.right, 2);
            rb.velocity = Vector2.right * moveSpeed;
            Debug.DrawRay(transform.position, Vector2.right * 2, Color.red);
             if (Physics2D.Raycast(transform.position, Vector2.right, 2, wallMask))
             {
                 Debug.Log("Hit wall");
                 movingLeft = true;
             }
             if (Physics2D.Raycast(transform.position, Vector2.right, 1, pitMask))
             {
                 Debug.Log("Hit pit");
                 movingLeft = true;
             }

            /*switch (hit.collider.tag)
            {
                case "Wall":
                    Debug.Log("Hit wall");
                    movingLeft = true;
                    break;

                case "Pit":
                    Debug.Log("Pit");
                    movingLeft = true;
                    break;

                default:
                    break;
            }*/
        }
    }
}
