﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum HealerState { follow, avoid, scared, moveToHeal, heal}
public class Healer : MonoBehaviour
{
    [SerializeField] private int healAmount;
    [SerializeField] private float maxDistanceFromPlayer;
    [SerializeField] private float maxDistanceFromEnemy;

    [SerializeField] private float speed;
    private Player2 player;
    private GameObject enemy;
    HealerState currentState = HealerState.follow;

    private float timeBtwHeal;
    [SerializeField] private float startTimeBtwHeal;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player2>();
        enemy = GameObject.FindGameObjectWithTag("Enemy");
    }

    // Update is called once per frame
    void Update()
    {
        switch (currentState)
        {
            case HealerState.follow:
                //följ spelaren

                //Debug.Log(Vector2.Distance(transform.position, player.transform.position));
                if(Vector2.Distance(transform.position, player.transform.position) > maxDistanceFromPlayer)
                {
                    Vector2 directionToPlayer = player.transform.position - transform.position;
                    transform.Translate(directionToPlayer * speed * Time.deltaTime);
                }
                //om helaren är för nära en fiende ska den undvika fienden
                if (enemy != null)
                {
                    if (Vector2.Distance(transform.position, enemy.transform.position) < maxDistanceFromEnemy)
                    {
                        currentState = HealerState.avoid;
                    }
                }
                break;

            case HealerState.avoid:
                Vector2 awayFromEnemy = enemy.transform.position + transform.position;
                transform.Translate(awayFromEnemy * speed * Time.deltaTime);
                if (Vector2.Distance(transform.position, enemy.transform.position) > maxDistanceFromEnemy + 5)
                {
                    currentState = HealerState.scared;
                }
                //om spealren är skadad rör sig helaren för att hela spelaren
                if (player.playerHealth < player.maxHealth)
                {
                    currentState = HealerState.moveToHeal;
                }
                break;

            case HealerState.scared:
                //healren gör ingenting;
                //om spealren är skadad rör sig helaren för att hela spelaren
                if (player.playerHealth < player.maxHealth)
                {
                    currentState = HealerState.moveToHeal;
                }
                if (enemy == null)
                {
                    currentState = HealerState.follow;
                }
                break;

            case HealerState.moveToHeal:
                Vector2 moveToPlayer = player.transform.position - transform.position;
                transform.Translate(moveToPlayer * speed * Time.deltaTime);
                //om healren är tillräckligt nära spelaren börjar den hela spelaren
                if (Vector2.Distance(transform.position, player.transform.position) <= maxDistanceFromPlayer)
                {
                    currentState = HealerState.heal;
                }
                break;

            case HealerState.heal:

                if (Vector2.Distance(transform.position, player.transform.position) <= maxDistanceFromPlayer)
                {
                    if (timeBtwHeal <= 0)
                    {
                        player.playerHealth += healAmount;
                        timeBtwHeal = startTimeBtwHeal;
                        if (player.playerHealth >= player.maxHealth)
                        {
                            currentState = HealerState.follow;
                        }
                    }
                }
                else
                {
                    currentState = HealerState.moveToHeal;
                }
                break;

        }
        if (timeBtwHeal > 0)
        {
            timeBtwHeal -= Time.deltaTime;
        }
    }
}
