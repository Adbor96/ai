﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour
{
    private Vector3 worldPosition;
    [SerializeField] private float speed;
    [SerializeField] private int damageAmount;
    // Start is called before the first frame update
    void Start()
    {
        //skapa en vector av musens position för att skjuta
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = Camera.main.nearClipPlane;
        worldPosition = Camera.main.ScreenToWorldPoint(mousePos);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, worldPosition, speed * Time.deltaTime);
        if(transform.position.x == worldPosition.x && transform.position.y == worldPosition.y)
        {
            Destroy(gameObject);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            collision.gameObject.GetComponent<Enemy>().Damage(damageAmount);
            Destroy(gameObject);
        }
    }
}
