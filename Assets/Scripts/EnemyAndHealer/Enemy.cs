﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum State { Idle, Chase, Shoot, Flee, Dead}
public class Enemy : MonoBehaviour
{
    State currentState = State.Idle;
    [SerializeField] private float detectionRadius;
    [SerializeField] private float shootRadius;
    [SerializeField] private float speed;
    private GameObject player;
    [SerializeField] private GameObject bullet;
    [SerializeField] private int enemyHealth;

    private float timeBtwShot;
    [SerializeField] private float startTimeBtwShot;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        timeBtwShot = startTimeBtwShot;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(currentState);
        

        switch (currentState)
        {
            case State.Idle:
                //om spelaren är inom synhåll av fienden börjar fienden jaga spelaren
                if (Vector2.Distance(transform.position, player.transform.position) < detectionRadius)
                {
                    currentState = State.Chase;
                }
                break;

            case State.Chase:
                //jaga spelaren
                Vector2 directionToPlayer = player.transform.position - transform.position;
                transform.Translate(directionToPlayer.normalized * speed * Time.deltaTime);
                //om spelaren är tillräckligt nära, skjut mot den
                if (Vector2.Distance(transform.position, player.transform.position) < shootRadius)
                    currentState = State.Shoot;
                break;

            case State.Shoot:
                //om fienden kan skjuta, skjut
                if (timeBtwShot <= 0)
                {
                    Debug.Log("Can shoot");
                    Shoot();
                    timeBtwShot = startTimeBtwShot;
                }

                if (Vector2.Distance(transform.position, player.transform.position) < detectionRadius)
                {
                    currentState = State.Chase;
                }
                break;

            case State.Flee:
                //fienden fly från spelaren
                Vector2 awayFromPlayer = player.transform.position + transform.position;
                transform.Translate(awayFromPlayer.normalized * speed * Time.deltaTime);
                break;

            case State.Dead:
                Die();
                break;
        }
        if(timeBtwShot > 0)
        {
            timeBtwShot -= Time.deltaTime;
        }
    }
    void Shoot()
    {
        Debug.Log("Firing");
        Instantiate(bullet, transform.position, Quaternion.identity);
    }
    public void Damage(int damageAmount)
    {
        enemyHealth -= damageAmount;
        if (enemyHealth > 20)
        {
            currentState = State.Chase;
        }
        else
        {
            currentState = State.Flee;
        }
        if(enemyHealth <= 0)
        {
            currentState = State.Dead;
        }
    }
    private void Die()
    {
        Destroy(gameObject);
    }
}
