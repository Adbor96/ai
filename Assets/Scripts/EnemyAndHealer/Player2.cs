﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player2 : MonoBehaviour
{
    public int playerHealth;
    public int maxHealth;
    private float x;
    private float y;
    [SerializeField] private float speed;

    private float timeBtwShot;
    [SerializeField] private float startTimeBtwShot;

    public Text hpText;

    [SerializeField] private GameObject bullet;
    // Start is called before the first frame update
    void Start()
    {
        maxHealth = playerHealth;
        
    }

    // Update is called once per frame
    void Update()
    {
        hpText.text = playerHealth.ToString();
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");

        Vector3 movementVector = new Vector3(x, y, 0);

        transform.position += movementVector * speed * Time.deltaTime;

        if (Input.GetMouseButtonDown(0))
        {
            if (timeBtwShot <= 0)
            {
                Shoot();
            }
        }
        if(timeBtwShot > 0)
        {
            timeBtwShot -= Time.deltaTime;
        }
    }

    public void Damage(int damageAmount)
    {
        playerHealth -= damageAmount;
    }
    public void Shoot()
    {
        Instantiate(bullet, transform.position, Quaternion.identity);
    }
    public void Arrested()
    {
        speed = 0;
        timeBtwShot += Time.deltaTime;
    }
}
