﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Herd : MonoBehaviour
{
    private Rigidbody2D rb;
    [SerializeField] private float speed;

    [SerializeField] private float flyToMouseWeight;
    [SerializeField] private float flyToAvoidMouseWeight;
    [SerializeField] private float cohesionWeight;
    [Range(0, 10)]
    [SerializeField] private float avoidanceWeight;
    [SerializeField] private float alignmentWeight;
    [SerializeField] private float sightRadius;
    [SerializeField] private float avoidRadius;
    [SerializeField] private float alignmentRadius;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void FixedUpdate()
    {
        Vector2 moveVelocity = Vector2.zero;
        //moveVelocity += AvoidMouse() * flyToAvoidMouseWeight;
        moveVelocity += FlyToMouse() * flyToMouseWeight;
        moveVelocity += Cohesion() * cohesionWeight;
        moveVelocity += Avoidance() * avoidanceWeight;
        moveVelocity += Alignment() * alignmentWeight;
        rb.velocity = moveVelocity.normalized;
        //gör så att up (om upp är framåt) är det hållet den rör sig åt
        transform.up = rb.velocity.normalized;
    }

    Vector2 FlyToMouse()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 direction = mousePos - (Vector2)transform.position;
        return direction.normalized;
    }

    Vector2 AvoidMouse()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 direction = (Vector2)transform.position - mousePos;
        return direction.normalized;
    }

    Vector2 Cohesion()
    {
        Collider2D[] nearbyCharacters = Physics2D.OverlapCircleAll(transform.position, sightRadius);
        Vector2 middlePosition = Vector2.zero;
        int amountOfCharacterNearby = 0;
        foreach(Collider2D character in nearbyCharacters)
        {
            Herd herd =  character.GetComponent<Herd>();
            if (herd != null && herd != this)
            {
                amountOfCharacterNearby++;
                middlePosition += (Vector2)herd.transform.position;

            }
            
        }
        //få mellanvärdet av alla karaktärers position
        middlePosition /= amountOfCharacterNearby;

        Vector2 direction = middlePosition - (Vector2)transform.position;
        Debug.DrawRay(transform.position, direction, Color.blue);
        return direction.normalized;
    }

    Vector2 Avoidance()
    {
        //ha max avstånd mellan olika "fjärilar"
        //ha kortare range för att se vilka fjärilar som är för nära
        //gå igenom alla fjärilar och åk i motsatt riktning från de som är för nära
        Collider2D[] nearbyCharacters = Physics2D.OverlapCircleAll(transform.position, sightRadius);
        Vector2 dir = Vector2.zero;

        foreach (Collider2D character in nearbyCharacters)
        {
            Herd herd = character.GetComponent<Herd>();
            if (herd != null && herd != this)
            {
                if(Vector2.Distance(character.transform.position, transform.position) < avoidRadius)
                {
                    Vector2 neighbourDir = (Vector2)herd.transform.position - (Vector2)transform.position;
                    //vector2's längd ska vara olika starka beroende på hur nära grannarna är
                    //neighbourDir ska bli ett värde mellan 0 och 1. 1 är väldigt nära, 0 är precis ute vid kanten
                    neighbourDir = neighbourDir.normalized * (1 - neighbourDir.magnitude / avoidRadius);
                    dir += -neighbourDir;
                }
            }

        }

        dir.Normalize();
        Debug.DrawRay(transform.position, dir, Color.red);
        return dir;
    }

    Vector2 Alignment()
    {
        //gå igenom alla vänner
        Collider2D[] neighbours = Physics2D.OverlapCircleAll(transform.position, sightRadius);
        Vector2 dir = Vector2.zero;

        foreach(Collider2D neighbour in neighbours)
        {
            Herd herd = neighbour.GetComponent<Herd>();
            if(herd != null)
            {
                dir += herd.GetComponent<Rigidbody2D>().velocity;
            }
        }
        //medelvärde av deras riktning, vart är de på väg
        //röra sig i samma riktning
        dir.Normalize();
        Debug.DrawRay(transform.position, dir, Color.green);
        return dir;
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, sightRadius);
        Gizmos.color = Color.black;
        Gizmos.DrawWireSphere(transform.position, avoidRadius);
        //Gizmos.color = Color.blue;
        //Gizmos.DrawWireSphere(transform.position, alignmentRadius);
    }

}
