﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private float x;
    private Rigidbody2D rb;
    [SerializeField] private float speed;
    private Collider2D playerCollider;
    [SerializeField] private LayerMask booMask;
    public bool facingRight;
    private SpriteRenderer spriteRend;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        playerCollider = GetComponent<Collider2D>();
        spriteRend = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        x = Input.GetAxisRaw("Horizontal");
        if (x > 0)
        {
            facingRight = true;
            spriteRend.flipX = false;
        }
        if(x < 0)
        {
            facingRight = false;
            spriteRend.flipX = true;
        }

        //Debug.DrawRay(playerCollider.bounds.center, Vector2.right * 5, Color.red);
        //RaycastHit2D hit = Physics2D.Raycast(playerCollider.bounds.center, Vector2.right, 5);
        //Debug.Log("Raycast hit: " + hit.transform.name);

        //if(Physics2D.Raycast(playerCollider.bounds.center)


    }
    private void FixedUpdate()
    {
        
        rb.velocity += Vector2.right * x * speed;
    }
}
