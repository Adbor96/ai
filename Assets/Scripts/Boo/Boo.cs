﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boo : MonoBehaviour
{
    private SpriteRenderer spriteRend;
    [SerializeField] private float speed;
    private CircleCollider2D booCollider;
    [SerializeField] private float radius;
    private Player player;
    private float distanceToPlayer;
    private float playerDistanceLength;

    public bool canChase;
    // Start is called before the first frame update
    void Start()
    {
        spriteRend = GetComponent<SpriteRenderer>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        speed = 1f;
        booCollider = GetComponent<CircleCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //avgör avståndet mellan Boo och spelaren
        Vector2 distanceVector = player.transform.position - transform.position;

        playerDistanceLength = distanceVector.magnitude;

        if(playerDistanceLength < 8)
        {
            if (player.facingRight)
            {
                if (transform.position.x > player.transform.position.x + -3)
                {
                    Hide();
                }
                else
                {
                    Chase();
                }
            }
            else
            {
                if(transform.position.x < player.transform.position.x + 3)
                {
                    Hide();
                }
                else
                {
                    Chase();
                }
            }
        }

    }
    public void Hide()
    {
        Debug.Log("Hide");
        canChase = false;
        spriteRend.color = new Color(1, 1, 1, 0.5f);
        booCollider.enabled = false;
    }
    public void UnHide()
    {
        Debug.Log("Unhide");
        canChase = true;
        spriteRend.color = new Color(1, 1, 1, 1);
    }
    private void Chase()
    {
        booCollider.enabled = true;
        spriteRend.color = new Color(1, 1, 1, 1);
        Vector2 directionToPlayer = player.transform.position - transform.position;
        directionToPlayer.Normalize();
        transform.Translate(directionToPlayer * Time.deltaTime);
    }
}
