﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject butterflyPrefab;
    public int flockSize = 10;
    List<GameObject> butterflies = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        Restart();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            Restart();
        }
    }

    void Restart()
    {
        if (butterflies.Count>0)
        {
            foreach (GameObject butterfly in butterflies)
            {
                Destroy(butterfly);
            }
            butterflies.Clear();
        }

        Vector2 min = Camera.main.ScreenToWorldPoint(new Vector2(0, 0));
        Vector2 max = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
        for (int i = 0; i < flockSize; i++)
        {
            Vector2 randomSpawnPos = new Vector2(Random.Range(min.x, max.x), Random.Range(min.x, max.x));
            Quaternion randomRotation = Quaternion.Euler(0, 0, Random.Range(0, 360f));
            GameObject instance = Instantiate(butterflyPrefab, randomSpawnPos, Quaternion.identity);
            butterflies.Add(instance);
        }


    }

}
