﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SleepInBed : UtilityAction
{
    public Image scoreBar;
    public float restSpeed = 1f;
    public override float CalculateUtilityScore(UtilityAgent agent)
    {
        float score = agent.tired;

        scoreBar.fillAmount =score;
        return score;
    }

    public override void PerformAction(UtilityAgent agent)
    {
        if (Vector2.Distance(agent.transform.position, transform.position) > 0.5f)
        {
            agent.MoveTowards(transform.position);
        }
        else
        {
            agent.tired -= restSpeed * Time.deltaTime;
            if (agent.tired <= 0)
            {
                agent.tired = 0;
                agent.currentAction = null;
            }
        }

       
    }
}
