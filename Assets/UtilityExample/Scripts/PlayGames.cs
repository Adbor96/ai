﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayGames : UtilityAction
{
    public float timeToPlay;
    float progress;
    public Image scoreBar;
    public Image progressBar;
    public override float CalculateUtilityScore(UtilityAgent agent)
    {
        float score = 0.5f;
        scoreBar.fillAmount = score;
        return score;
    }

    public override void PerformAction(UtilityAgent agent)
    {
        if (Vector2.Distance(agent.transform.position, transform.position) > 0.5f)
        {
            agent.MoveTowards(transform.position);
        }
        else
        {
            progress += Time.deltaTime;
            if (progress >= timeToPlay)
            {
                agent.currentAction = null;
                progress = 0;
            }
            progressBar.fillAmount = progress/timeToPlay;
        }
    }
}
