﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EatFood : UtilityAction
{
    public float timeToEat;
    float eatingProgress;
    public float hungerReduction;
    public Image scoreBar;
    public Image progressBar;
    public AnimationCurve utilityCurve;
    public override float CalculateUtilityScore(UtilityAgent agent)
    {
        float score = utilityCurve.Evaluate(agent.hungry);
        scoreBar.fillAmount = score;
        return score;
    }

    public override void PerformAction(UtilityAgent agent)
    {
        if (Vector2.Distance(agent.transform.position, transform.position) > 0.5f)
        {
            agent.MoveTowards(transform.position);
        }
        else
        {
           eatingProgress += Time.deltaTime;
            if (eatingProgress >= timeToEat)
            {
                agent.hungry -= hungerReduction;
                if (agent.hungry < 0)
                {
                    agent.hungry = 0;
                }
                agent.currentAction = null;
                eatingProgress = 0;
            }
            progressBar.fillAmount = eatingProgress/timeToEat;
        }
    }

}
