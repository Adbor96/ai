﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SleepOnFloor : UtilityAction
{
    public AnimationCurve utilityCurve;
    public float restSpeed = 0.5f;

    public override float CalculateUtilityScore(UtilityAgent agent)
    {
        float score = utilityCurve.Evaluate(agent.tired);
        return score;
    }

    public override void PerformAction(UtilityAgent agent)
    {
        agent.GetComponent<SpriteRenderer>().color = Color.red;
        agent.tired -= restSpeed * Time.deltaTime;

        if (agent.tired < 0.5f)
        {
            agent.currentAction = null;
            agent.GetComponent<SpriteRenderer>().color = Color.white;
        }
    }


}
