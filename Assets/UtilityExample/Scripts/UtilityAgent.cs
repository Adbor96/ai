﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UtilityAgent : MonoBehaviour
{
    public float moveSpeed;

    public float tired;
    public Image tiredBar;
    public float hungry;
    public Image hungryBar;

    public UtilityAction currentAction;

    public List<UtilityAction> availableActions;

    // Update is called once per frame
    void Update()
    {
        tired += Time.deltaTime * 0.02f;
        hungry += Time.deltaTime * 0.02f;

        hungryBar.fillAmount = hungry;
        tiredBar.fillAmount = tired;

        if (currentAction == null)
        {
            ChooseAction();
        }
        else
        {
            currentAction.PerformAction(this);
        }
    }

    void ChooseAction()
    {
        Debug.Log("Choosing new action");
        float bestScore = 0;
        UtilityAction bestAction = null;
        for (int i = 0; i < availableActions.Count; i++)
        {
            float score = availableActions[i].CalculateUtilityScore(this);
            if (score > bestScore)
            {
                bestScore = score;
                bestAction = availableActions[i];
            }
        }
        currentAction = bestAction;
    }

    public void MoveTowards(Vector2 pos)
    {
        transform.position = Vector2.MoveTowards(transform.position, pos, moveSpeed*Time.deltaTime);
    }

}
