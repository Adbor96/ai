﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UtilityAction : MonoBehaviour
{
    

    public virtual float CalculateUtilityScore(UtilityAgent agent)
    {
        return -1;
    }

    public virtual void PerformAction(UtilityAgent agent)
    { }
}
