﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StageSelect : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void BossScene()
    {
        SceneManager.LoadScene("BossBattle");
    }
    public void FlockingScene()
    {
        SceneManager.LoadScene("Prey");
    }
    public void EnemyAndHealer()
    {
        SceneManager.LoadScene("EnemyAndHealer");
    }
    public void Utility()
    {
        SceneManager.LoadScene("Utility");
    }
}
